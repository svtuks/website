from flask import Flask, render_template, redirect, request, session, Blueprint, abort
from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileRequired, FileAllowed
from wtforms import StringField, PasswordField, SubmitField, IntegerField, SelectField, BooleanField, validators

import bcrypt
import os
import cfg

stuks_bp = Blueprint('stuks_bp', __name__, template_folder='templates', static_folder='static')

from models.stuks import User, Product, Purchases, db


class UnlockForm(FlaskForm):
    password = PasswordField("Password", render_kw={"placeholder": "Password"})
    submit = SubmitField("Unlock")


class AddUserForm(FlaskForm):
    firstname = StringField("Firstname", validators=[validators.Length(min=2, max=20, message="Username has to be between 2 and 20 characters!")], render_kw={"placeholder": "First name"})
    lastname = StringField("Lastname", validators=[validators.Length(min=2, max=20, message="Username has to be between 2 and 20 characters!")], render_kw={"placeholder": "Last name"})
    email = StringField("Email", validators=[validators.Email()], render_kw={"placeholder": "E-mail"})
    image = FileField("Image", validators=[FileAllowed(["png", "jpg", "jpeg"], "PNG, JPG or JPEG images only!")])
    is_admin = BooleanField("Admin")
    is_active = BooleanField("Active", default=True)
    submit = SubmitField("Submit")


class AddProductForm(FlaskForm):
    name = StringField("Name", validators=[validators.Length(min=2, max=20, message="Product name has to be between 2 and 20 characters!")], render_kw={"placeholder": "Product name"})
    category = SelectField("Category", choices=[("drinks", "Drinks"), ("snacks", "Snacks")])
    price = IntegerField("Price", render_kw={"placeholder": "Price in cents"})
    image = FileField("Image", validators=[FileAllowed(["png"], "PNG images only!")])
    submit = SubmitField("Submit")


def get_buy_list(buy_list_id):
    """Function to retrieve products from the database, based on a list with product ID's"""

    return [Product.query.get(product_id) for product_id in buy_list_id]


def get_buy_list_price(buy_list_id):
    """Function to calculate the sum of products in a user's buy list'"""

    return sum([product.price for product in get_buy_list(buy_list_id)])


@stuks_bp.route("/products/<category>", methods=["GET", "POST"])
def products(category):
    """Get and show all the available products in a specific category."""

    if not session.get("user_id", None):
        return redirect("login")

    user = User.query.get(session['user_id'])
    products = Product.query.filter_by(category=category).filter_by(is_available=True).order_by(Product.name).all()
    buy_list = get_buy_list(session['buy_list_ids'])

    return render_template("stuks_bp/products.html", user=user, products=products, buy_list=buy_list)


@stuks_bp.route("/buy/<int:product_id>")
def buy(product_id):
    """Add the selected product to your shopping cart. Function will return back to the previous page."""

    if not session.get("user_id", None):
        return redirect("login")

    user = User.query.get(session['user_id'])
    total_price = get_buy_list_price(session['buy_list_ids']) + Product.query.get(product_id).price

    session['buy_list_ids'].append(product_id)
    session.modified = True

    # Redirect back to the page you were coming from
    return redirect(request.referrer)


@stuks_bp.route("/trash/<int:index>")
def trash(index):
    if not session.get("user_id", None):
        return redirect("login")

    session['buy_list_ids'].pop(index)
    session.modified = True

    # Redirect back to the page you were coming from
    return redirect(request.referrer)


@stuks_bp.route("/buyout")
def buyout():
    """Put your purchases in the database and update your balance and the stock of the products."""

    if not session.get("user_id", None):
        return redirect("login")

    user = User.query.get(session['user_id'])

    for product_id in session['buy_list_ids']:
        purchase = Purchases()
        purchase.user_id = session['user_id']
        purchase.product_id = product_id

        db.session.add(purchase)

        product = Product.query.get(product_id)

        user.balance -= product.price
        product.stock -= 1

        db.session.commit()

    session['buy_list_ids'].clear()

    return redirect("/stuks/logout")


@stuks_bp.route("/admin")
def admin():
    if not session.get("user_id", None):
        return redirect("login")

    user = User.query.get(session['user_id'])

    if not user.is_admin:
        return redirect("products/drinks")

    buy_list = get_buy_list(session['buy_list_ids'])

    return render_template("stuks_bp/admin.html", user=user, buy_list=buy_list)


@stuks_bp.route("/stock")
def stock():
    if not session.get("user_id", None):
        return redirect("login")

    user = User.query.get(session['user_id'])

    if not user.is_admin:
        return redirect("products/drinks")

    buy_list = get_buy_list(session['buy_list_ids'])

    return render_template("stuks_bp/stock_category.html", user=user, buy_list=buy_list)


@stuks_bp.route("/stock/<category>", methods=["GET", "POST"])
def edit_stock(category):
    """Page to edit the stock of the products, based on product category."""

    if not session.get("user_id", None):
        return redirect("login")

    user = User.query.get(session['user_id'])

    if not user.is_admin:
        return redirect("products/drinks")

    if request.method == "POST":
        for product_id, stock_edit_value in request.form.items():

            product = Product.query.get(int(product_id))
            product.stock += int(stock_edit_value)

            db.session.commit()

    products = Product.query.filter_by(category=category).all()

    return render_template("stuks_bp/stock.html", user=user, products=products, buy_list=get_buy_list(session['buy_list_ids']))


@stuks_bp.route("/add_product", methods=["GET", "POST"])
def add_product():
    """Page for the admins to add a product to the database."""

    if not session.get("user_id", None):
        return redirect("login")

    user = User.query.get(session['user_id'])

    if not user.is_admin:
        return redirect("products/drinks")

    form = AddProductForm()

    if form.validate_on_submit():
        new_product = Product()
        
        new_product.name = form.name.data
        new_product.category = form.category.data
        new_product.price = form.price.data
        
        db.session.add(new_product)
        db.session.commit()
        db.session.flush()
    
        # Save the product image as the product ID
        if image := form.image.data:
            filename = str(new_product.id) + ".png"
            image.save(os.path.join("stuks/static/img/", "product_img", filename))
        
        return redirect("add_product")

    return render_template("stuks_bp/add_product.html", form=form, user=user, buy_list=get_buy_list(session['buy_list_ids']))


@stuks_bp.route("/hide_product/<int:product_id>")
def hide_product(product_id):
    if not session.get("user_id", None):
        return redirect("login")

    user = User.query.get(session['user_id'])

    if not user.is_admin:
        return redirect("products/drinks")

    product = Product.query.get(product_id)
    product.is_available = False
    db.session.commit()

    return redirect(request.referrer)


@stuks_bp.route("/restore_product")
def restore_product():
    if not session.get("user_id", None):
        return redirect("login")

    user = User.query.get(session['user_id'])

    if not user.is_admin:
        return redirect("products/drinks")

    products = Product.query.filter_by(is_available=False).order_by(Product.name).all()

    return render_template("stuks_bp/restore_product.html", user=user, products=products)


@stuks_bp.route("/user/<int:id>")
def user(id):
    if not session.get("user_id", None):
        return redirect("/stuks/login")

    user = User.query.get(session.get("user_id"))

    if not user:
        return redirect("/stuks/login")

    purchases = Purchases.query.filter_by(user_id=user.id).order_by(Purchases.datetime.desc()).limit(20).all()
    
    full_purchases = []
    for purchase in purchases:
        purchase_info = Product.query.get(purchase.product_id)

        full_purchases.append((purchase.datetime, purchase_info,))

    return render_template("stuks_bp/user.html", user=user, purchases=full_purchases)


@stuks_bp.route("/restore_product/<int:product_id>")
def restore_product_by_id(product_id):
    if not session.get("user_id", None):
        return redirect("login")

    user = User.query.get(session['user_id'])

    if not user.is_admin:
        return redirect("products/drinks")

    product = Product.query.get(product_id)
    product.is_available = True
    db.session.commit()

    return redirect(request.referrer)



@stuks_bp.route("/edit_product/<int:product_id>", methods=["GET", "POST"])
def edit_product(product_id):
    """Page for the admins to edit a product."""

    if not session.get("user_id", None):
        return redirect("login")

    user = User.query.get(session['user_id'])

    if not user.is_admin:
        return redirect("products/drinks")

    product = Product.query.get(product_id)
    form = AddProductForm(obj=product)

    if form.validate_on_submit():
        if image := form.image.data:
            filename = str(product.id) + ".png"
            image.save(os.path.join("stuks/static/img/", "product_img", filename))

        product.name = form.name.data
        product.category = form.category.data
        product.price = form.price.data

        db.session.commit()

    return render_template("stuks_bp/add_product.html", user=user, form=form, buy_list=get_buy_list(session['buy_list_ids']))


@stuks_bp.route("/add_user", methods=["GET", "POST"])
def add_user():
    """Page for the admins to add a user."""

    if not session.get("user_id", None):
        return redirect("login")

    user = User.query.get(session['user_id'])

    if not user.is_admin:
        return redirect("products/drinks")

    form = AddUserForm()

    if form.validate_on_submit():
        new_user = User()

        new_user.firstname = form.firstname.data
        new_user.lastname = form.lastname.data
        new_user.email = form.email.data
        new_user.is_admin = form.is_admin.data
        new_user.is_active = form.is_active.data

        db.session.add(new_user)
        db.session.commit()
        db.session.flush()

        if image := form.image.data:
            filename = str(new_user.id) + ".png"
            image.save(os.path.join("stuks/static/img/", "user_img", filename))

        return redirect("add_user")

    return render_template("stuks_bp/add_user.html", user=user, form=form, buy_list=get_buy_list(session['buy_list_ids']))


@stuks_bp.route("/edit_user")
def edit_users():
    if not session.get("unlocked", False):
        return redirect("unlock")

    if not session.get("user_id", None):
        return redirect("login")

    user = User.query.get(session['user_id'])

    if not user.is_admin:
        return redirect("products/drinks")

    users = User.query.order_by(User.firstname).all()

    return render_template("stuks_bp/edit_user.html", user=user, users=users, buy_list=get_buy_list(session['buy_list_ids']))


@stuks_bp.route("/edit_user/<int:user_id>", methods=["GET", "POST"])
def edit_user(user_id):
    if not session.get("unlocked", False):
        return redirect("unlock")

    if not session.get("user_id", None):
        return redirect("login")

    user = User.query.get(session['user_id'])

    if not user.is_admin:
        return redirect("products/drinks")

    edit_user = User.query.get(user_id)
    form = AddUserForm(obj=edit_user)

    if form.validate_on_submit():

        if image := form.image.data:
            filename = str(edit_user.id) + ".png"
            image.save(os.path.join("stuks/static/img/", "user_img", filename))

        edit_user.firstname = form.firstname.data
        edit_user.lastname = form.lastname.data
        edit_user.email = form.email.data
        edit_user.is_admin = form.is_admin.data
        edit_user.is_active = form.is_active.data

        db.session.commit()
        return redirect("/stuks/edit_user")

    return render_template("stuks_bp/add_user.html", user=user, form=form, buy_list=get_buy_list(session['buy_list_ids']))


@stuks_bp.route("/balances", methods=["GET", "POST"])
def balances():
    if not session.get("user_id", None):
        return redirect("login")

    user = User.query.get(session['user_id'])

    if not user.is_admin:
        return redirect("products/drinks")

    if request.method == "POST":
        for user_id, balance_edit_value in request.form.items():

            edit_user = User.query.get(int(user_id))
            edit_user.balance += int(balance_edit_value)

            db.session.commit()

    users = User.query.filter_by(is_active=True).order_by(User.firstname).all()   

    return render_template("stuks_bp/balances.html", user=user, users=users, buy_list=get_buy_list(session['buy_list_ids']))    


@stuks_bp.route("/login")
def login():
    if not session.get("unlocked", False):
        return redirect("unlock")

    users = User.query.filter_by(is_active=True).order_by(User.firstname).all()
    last_purchases = db.session.query(User.id, User.firstname, User.lastname, Product.name).select_from(Purchases).join(User).join(Product).filter(Purchases.user_id==User.id).order_by(Purchases.datetime.desc()).limit(5).all()

    return render_template("stuks_bp/login.html", users=users, purchases=last_purchases)


@stuks_bp.route("/login/<int:user_id>")
def login_user(user_id):
    if not session.get("unlocked", False):
        return redirect("unlock")

    user = User.query.get(user_id)

    if user.is_admin:
        return redirect(f"/stuks/login/admin/{user_id}")

    session['user_id'] = user_id
    session['buy_list_ids'] = []

    return redirect("/stuks/products/drinks")


@stuks_bp.route("/login/admin/<int:user_id>", methods=["GET", "POST"])
def admin_login(user_id):
    form = UnlockForm()

    if request.method == "POST":
        if bcrypt.checkpw(form.password.data.encode('utf-8'), cfg.stuks_admin_password):
            session['user_id'] = user_id
            session['buy_list_ids'] = []
            return redirect("/stuks/products/drinks")

    return render_template("stuks_bp/unlock.html", form=form, admin_login=True)


@stuks_bp.route("/logout")
def logout():
    if not session.get("unlocked", False):
        return redirect("unlock")

    session['user_id'] = None
    return redirect("login")


@stuks_bp.route("/unlock", methods=["GET", "POST"])
def index():
    form = UnlockForm()

    if request.method == "POST":
        if bcrypt.checkpw(form.password.data.encode('utf-8'), cfg.stuks_unlock_password):
            session['unlocked'] = True
            return redirect("login")

    return render_template("stuks_bp/unlock.html", form=form, admin_login=False)


@stuks_bp.route("/lock")
def lock():
    session["unlocked"] = False
    session["user_id"] = None
    session["buy_list_ids"] = None

    return redirect("unlock")


@stuks_bp.route("/")
def home():
    return redirect("logout")