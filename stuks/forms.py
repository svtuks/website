from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileRequired, FileAllowed
from wtforms import StringField, PasswordField, SubmitField, IntegerField, SelectField, BooleanField, validators


class UnlockForm(FlaskForm):
    password = PasswordField("Password", render_kw={"placeholder": "Password"})
    submit = SubmitField("Unlock")


class AddUserForm(FlaskForm):
    firstname = StringField("Firstname", validators=[validators.Length(min=2, max=20, message="Username has to be between 2 and 20 characters!")], render_kw={"placeholder": "First name"})
    lastname = StringField("Lastname", validators=[validators.Length(min=2, max=20, message="Username has to be between 2 and 20 characters!")], render_kw={"placeholder": "Last name"})
    email = StringField("Email", validators=[validators.Email()], render_kw={"placeholder": "E-mail"})
    image = FileField("Image", validators=[FileAllowed(["png"], "PNG images only!")])
    is_admin = BooleanField("Admin")
    is_active = BooleanField("Active", default=True)
    submit = SubmitField("Submit")

class AddProductForm(FlaskForm):
    name = StringField("Name", validators=[validators.Length(min=2, max=20, message="Product name has to be between 2 and 20 characters!")], render_kw={"placeholder": "Product name"})
    category = SelectField("Category", choices=[("drinks", "Drinks"), ("snacks", "Snacks")])
    price = IntegerField("Price", render_kw={"placeholder": "Price in cents"})
    image = FileField("Image", validators=[FileAllowed(["png"], "PNG images only!")])
    submit = SubmitField("Submit")
