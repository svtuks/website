"""empty message

Revision ID: 376430fe13c6
Revises: 49c6765b2886
Create Date: 2022-03-01 21:36:20.961119

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '376430fe13c6'
down_revision = '49c6765b2886'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('event',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('title', sa.String(length=255), nullable=False),
    sa.Column('date', sa.Date(), nullable=False),
    sa.Column('time', sa.Time(), nullable=False),
    sa.Column('description', sa.Text(), nullable=False),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('event')
    # ### end Alembic commands ###
