from flask import Flask, render_template, request, session, redirect, url_for, flash
from flask_sqlalchemy import SQLAlchemy
from flask_wtf import FlaskForm
from wtforms import StringField, TextAreaField, SubmitField, ValidationError
from wtforms.validators import InputRequired, Length
from wtforms.fields.html5 import DateField
from wtforms_components import TimeField
from datetime import date
import os
import flask_migrate


# Create a local cfg file if it doesn't exist yet
if not os.path.exists("cfg.py"):
    with open('cfg.py', 'w') as file:
        file.write('''
db = "mysql://tuks-site:mahb384twx@127.0.0.1:33061/tuks-site"
stuks_unlock_password = b'$2b$12$7C7kPiHDex1TkaCUMzSAQ.1XkdjzsY4kKuMP/SPa//Jn9bNweCcpS' # test
stuks_admin_password = b'$2b$12$OI4wXI0aJ7/d1BMorqIz3u1xKZYkFfPrDVAzxdIJ8YVoYsYfqQxvu' # admin
''')

import cfg


################## CONFIGURATION #################
app = Flask(__name__)
app.config['TEMPLATES_AUTO_RELOAD'] = True
app.config['SECRET_KEY'] = "fs3qnisfsmvfswe"

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_DATABASE_URI'] = cfg.db
db = SQLAlchemy(app)






# Define the migrator, used by 'flask db upgrade'
migrate = flask_migrate.Migrate(app, db)

from stuks.stuks_bp import stuks_bp
app.register_blueprint(stuks_bp, url_prefix='/stuks', template_folder='stuks/templates', static_folder='stuks/static')

################## Template filters #################
@app.template_filter('dutch_month')
def format_date(given_date):
    months = {1: 'januari', 2: 'februari', 3: 'maart', 4: 'apr', 5: 'mei', 6: 'juni', 7: 'juli', 8: 'augustus', 9: 'september', 10: 'oktober', 11: 'november', 12: 'december'}
    return months[int(given_date.strftime('%m'))]

@app.template_filter('dutch_day')
def format_dutch_day(given_date):
    days = {0: 'zo', 1: 'ma', 2: 'di', 3: 'wo', 4: 'do', 5: 'vr', 6: 'za'}
    return days[int(given_date.strftime('%w'))]

@app.template_filter('time')
def format_time(given_time):
    return given_time.strftime('%H:%M')



################# FORM MODELS #################
class EventForm(FlaskForm):
    title = StringField('Title', validators=[InputRequired(), Length(min=5, max=255, message="Title must be between 15 and 255 characters")])
    date = DateField('Date', format='%Y-%m-%d', validators=[InputRequired()])
    time = TimeField('Time', validators=[InputRequired()])
    description = TextAreaField('Description', validators=[InputRequired()])
    submit = SubmitField('Save')



################## DATABASE MODELS #################
class Event(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(255), nullable=False)
    date = db.Column(db.Date, nullable=False)
    time = db.Column(db.Time, nullable=False)
    description = db.Column(db.Text, nullable=False)    



################## ROUTES #################
@app.route("/", methods=["GET", "POST"])
def index():
    # Get all events that are planned past today
    events = Event.query.order_by(Event.date).filter(Event.date >= date.today()).all()
    return render_template("index.html", current_page="home", events=events)


@app.route("/admin")
def admin(): 
    # events = Event.query.order_by(Event.date).all()
    # return render_template("admin.html", events=events)
    return redirect("https://www.youtube.com/watch?v=dQw4w9WgXcQ")

@app.route("/<int:event_id>", methods=['GET'])
def event(event_id):
    event = Event.query.get(event_id)
    return render_template("admin.html", event=event)

@app.route("/<int:event_id>/edit", methods=['GET', 'POST'])
def edit_event(event_id):
    event = Event.query.get(event_id)
    form = EventForm(obj=event)

    if form.validate_on_submit():
        form.populate_obj(event)
        db.session.commit()
        return redirect(url_for("admin"))
    
    return render_template("admin.html", form=form, action=f'/{event_id}/edit',method='POST')

@app.route("/<int:event_id>/delete", methods=['POST'])
def delete_event(event_id):
    event = Event.query.get(event_id)
    db.session.delete(event)
    db.session.commit()
    flash(f"Event {event.title} deleted successfully")
    return redirect(url_for("admin"))

@app.route("/add_event", methods=['GET', 'POST'])
def add_event():
    # A small implementation to get events into the DB.
    # To be replaced by a proper administrator environment behind a login wall.
    event = Event()
    form = EventForm(obj=event)

    if form.validate_on_submit():
        form.populate_obj(event)
        db.session.add(event)
        db.session.commit()
        return redirect(url_for("admin"))
    
    return render_template("admin.html", form=form, method='POST')