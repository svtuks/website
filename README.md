### Running

To run this project, you'll need:
- Linux or OSX (or something similarly Posix compliant)
- Python 3.6+
- Poetry
- MariaDB (or MySQL) client libraries (Manjaro: install `mariadb-libs` package)
- Docker (Manjaro: install the `docker` package and then enable and start it using `sudo systemctl enable docker && sudo systemctl start docker`)

Running `./start` should be enough to get a development server up and running on `http://localhost:5000`.

### Database

Starting the server will automatically create a `cfg.py` configuration file preset to use SQLite as a database. You can modify it in your local installation to use MySQL instead.

After modifying database models, you need to create a new database migration using `./start db-revision`. Inspect the generated file to make sure it's correct. Then apply it to your local database using `./start db-upgrade`. Migration files should be commit to git, and will be automatically applied when deploying.

### Deploying

`./start deploy dev` and `./start deploy prod` will attempt to deploy to the development and the production environment respectively. Your public ssh key and your IP address need to be [authorized by Antagonist DirectAdmin](https://tuks.online:2223/user/plugins/ssh).

### Notable files and directories

| Path              | Description |
| -----             | --------- |
| cfg.py            | Local settings. Restart your server after modifying. |
| start             | The start script. Use `./start help` to view supported commands. |
| migrations/       | Database migrations, managed by Alembic. |
